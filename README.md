# BAUNZ HAUS
BAUNZ HAUS is a simple game:

  1. Get the ball into the box
  2. Confetti
  3. ???
  4. Profit

BAUNZ HAUS was written using SpriteKit physics, custom level loading and handling, and senioritis.

## Dependencies
BAUNZ HAUS makes use of [Carthage](https://github.com/Carthage/Carthage) and [Mantle](https://github.com/Mantle/Mantle). Against all common sense, these are included in the repo, so things should clone correctly without needing to install Carthage on a client machine (fingers crossed.)

BAUNZ HAUS uses the "Thumbnails" icon from [https://icons8.com].

## Contributors
Written "with love" by

  * Chris Beard
  * Itai Ferber
