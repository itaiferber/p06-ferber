#import "BHSGameViewController.h"
#import "BHSGameScene.h"
#import "UIColor+HexRepresentation.h"

#pragma mark Private Interface
@interface BHSGameViewController ()

#pragma mark - Private Properties
//! The loaded game levels.
@property NSArray<BHSLevel *> *levels;

//! The index of the current level in the levels array.
@property NSUInteger level;

@end

#pragma mark -
@implementation BHSGameViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load levels.
    NSURL *levelDataURL = [[NSBundle mainBundle] URLForResource:@"level_data" withExtension:@"json"];
    if (!levelDataURL) {
        NSLog(@"Unable to get level data URL.");
        return;
    }

    NSError *error = nil;
    self.levels = [BHSLevel levelsWithContentsAtURL:levelDataURL error:&error];
    NSAssert(self.levels.count > 0, @"Unable to load game levels: %@", error);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.level = 0;
    BHSGameScene *scene = [BHSGameScene sceneWithSize:self.view.bounds.size level:self.levels[self.level]];
    scene.gameDelegate = self;
    [self.gameView presentScene:scene];
}

#pragma mark - Presenting Levels
- (void)presentLevel:(NSUInteger)level transitionDirection:(SKTransitionDirection)direction
{
    self.level = level;

    BHSGameScene *scene = [BHSGameScene sceneWithSize:self.view.bounds.size level:self.levels[self.level]];
    scene.gameDelegate = self;
    [self.gameView presentScene:scene transition:[SKTransition pushWithDirection:direction duration:0.5]];
}

#pragma mark - BHSGameDelegate Methods
- (void)levelInSceneDidEnd:(BHSGameScene *)scene
{
    self.levelPickerButton.userInteractionEnabled = NO;
    scene.gameDelegate = nil;

    NSUInteger nextLevel = self.level + 1;
    if (nextLevel >= self.levels.count) {
        nextLevel = 0;
    }

    [self presentLevel:nextLevel transitionDirection:SKTransitionDirectionLeft];
    self.levelPickerButton.userInteractionEnabled = YES;
}

#pragma mark - Actions
- (IBAction)presentLevelPicker:(UIButton *)levelPickerButton
{
    self.levelPickerButton.userInteractionEnabled = NO;
    [self.levelPicker reloadData];
    
    CGRect frame = CGRectInset(self.view.frame, 10, 10);
    frame.origin.y = frame.size.height;
    self.levelPicker.frame = frame;
    self.levelPicker.hidden = NO;

    frame = CGRectInset(self.view.frame, 10, 10);
    [UIView animateWithDuration:0.25 animations:^{
        self.levelPicker.frame = frame;
    }];
}

#pragma mark - UICollectionViewDataSource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return collectionView == self.levelPicker && section == 0 ? (NSInteger)self.levels.count : -1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView != self.levelPicker || indexPath.section > 0) {
        return nil;
    }

    static NSString * const BHSGameViewControllerLevelPickerCellIdentifier = @"BHSGameViewControllerLevelPickerCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BHSGameViewControllerLevelPickerCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UICollectionViewCell alloc] initWithFrame:CGRectZero];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        [label.centerXAnchor constraintEqualToAnchor:cell.centerXAnchor].active = YES;
        [label.centerYAnchor constraintEqualToAnchor:cell.centerYAnchor].active = YES;
        [label.widthAnchor constraintEqualToAnchor:cell.widthAnchor].active = YES;
        [label.heightAnchor constraintEqualToAnchor:cell.heightAnchor].active = YES;

        label.textColor = [UIColor colorWithHexRepresentation:@"#2c3e50" error:NULL];
        [cell.contentView addSubview:label];
    }

    UILabel *label = (UILabel *)[cell.contentView subviews][0];
    label.text = [NSString stringWithFormat:@"%ld", (long)indexPath.item + 1];

    if ((NSUInteger)indexPath.item == self.level) {
        cell.layer.borderColor = [UIColor colorWithHexRepresentation:@"#3498db" error:NULL].CGColor;
        cell.layer.borderWidth = 3.0;
    } else {
        cell.layer.borderWidth = 0.0;
    }

    return cell;
}

#pragma mark - UICollectionViewDelegate Methods
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.layer.borderColor = [UIColor colorWithHexRepresentation:@"#f1c40f" error:NULL].CGColor;
    cell.layer.borderWidth = 3.0;
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    if ((NSUInteger)indexPath.item == self.level) {
        cell.layer.borderColor = [UIColor colorWithHexRepresentation:@"#3498db" error:NULL].CGColor;
        cell.layer.borderWidth = 3.0;
    } else {
        cell.layer.borderWidth = 0.0;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger level = (NSUInteger)indexPath.item;

    [UIView animateWithDuration:0.25 animations:^{
        self.levelPicker.alpha = 0;
    } completion:^(BOOL finished) {
        self.levelPicker.hidden = YES;
        self.levelPicker.alpha = 1;

        if (self.level != level) {
            [self presentLevel:level transitionDirection:SKTransitionDirectionUp];
        }

        self.levelPickerButton.userInteractionEnabled = YES;
    }];
}

@end
