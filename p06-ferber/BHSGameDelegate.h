@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@class BHSGameScene;
@protocol BHSGameDelegate <NSObject>

/*!
 Called when the player wins the current level in the given scene.
 
 \param scene The scene the level is being displayed in.
 */
- (void)levelInSceneDidEnd:(BHSGameScene *)scene;

@end

NS_ASSUME_NONNULL_END
