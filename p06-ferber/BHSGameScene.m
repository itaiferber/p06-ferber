#import "BHSGameScene.h"
#import "UIColor+HexRepresentation.h"

#pragma mark Constants
//! A the radius of the ball to use.
static CGFloat const BHSBallRadius = 12;

#pragma mark - Physics Categories
NS_OPTIONS(uint32_t, BHSPhysicsCategory) {
    BHSPhysicsCategoryBall       = 1 << 0,
    BHSPhysicsCategoryObstacle   = 1 << 1,
    BHSPhysicsCategoryGoal       = 1 << 2,
    BHSPhysicsCategoryLaunchArea = 1 << 3,
};

#pragma mark - Helper Functions
/*!
 Returns whether two points are approximately equal.
 
 Two points are considered approximately equal if they are within half an x and
 y unit of one another.
 */
static inline BOOL BHSPointsApproximatelyEqual(CGPoint p1, CGPoint p2)
{
    return fabs(p1.x - p2.x) <= 0.5 && fabs(p1.y - p2.y) <= 0.5;
}

#pragma mark - Private Interface
@interface BHSGameScene ()

#pragma mark - Private Properties
//! The ball interacting with the view.
@property (nullable) SKShapeNode *ball;

//! The level's floor.
@property (nullable) SKShapeNode *floor;

//! The node the user drags from to launch the ball.
@property SKShapeNode *launchArea;

//! The node the user is trying to get the ball into.
@property SKShapeNode *goal;

/*!
 The node that holds the inner part of the goal the user is trying to get the
 ball into.
 */
@property SKShapeNode *innerGoal;

//! The current touch in the view.
@property (nullable) UITouch *currentTouch;

//! The path drawn for the trajectory of the ball.
@property (nullable) SKShapeNode *trajectory;

//! Whether to be tracking the location of the ball (for victory condition).
@property BOOL trackBallLocation;

@end

#pragma mark -
@implementation BHSGameScene

#pragma mark - Initialization
+ (instancetype)sceneWithSize:(CGSize)size level:(BHSLevel *)level
{
    return [[self alloc] initWithSize:size level:level];
}

- (instancetype)initWithSize:(CGSize)size level:(BHSLevel *)level
{
    if (!level) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"Given level must not be nil."
                                     userInfo:nil];
    } else if (!(self = [super initWithSize:size])) {
        return nil;
    }

    // Preload sound files so we don't incur a performance penalty the first
    // time we play them.
    (void)[SKAction playSoundFileNamed:@"tap.caf" waitForCompletion:NO];
    (void)[SKAction playSoundFileNamed:@"success.caf" waitForCompletion:NO];

    // Scale the actual scene to fill the view we're in; we'll resize our
    // object distances to match.
    self.scaleMode = SKSceneScaleModeResizeFill;

    _currentLevel = level;
    self.backgroundColor = level.backgroundColor;

    // Set up the launch area.
    _launchArea = [level.launchArea copy];
    CGFloat pattern[] = {5.0, 2.0};
    CGPathRef dashed = CGPathCreateCopyByDashingPath(_launchArea.path, NULL, 1, pattern, 2);
    SKShapeNode *dashedNode = [SKShapeNode shapeNodeWithPath:dashed centered:YES];
    CGPathRelease(dashed);

    dashedNode.strokeColor = _launchArea.strokeColor;
    dashedNode.fillColor = [UIColor colorWithWhite:1.0f alpha:0.4f];
    dashedNode.lineWidth = _launchArea.lineWidth;
    [_launchArea addChild:dashedNode];

    _launchArea.lineWidth = 0;
    _launchArea.physicsBody.categoryBitMask = BHSPhysicsCategoryLaunchArea;
    _launchArea.position = [self scaledPosition:_launchArea.position];
    _launchArea.zPosition = -2;
    [self addChild:_launchArea];

    // Set up the goal.
    _goal = [level.goal copy];
    _goal.position = [self scaledPosition:_goal.position];
    _goal.physicsBody.categoryBitMask = BHSPhysicsCategoryObstacle;
    [self addChild:_goal];

    _innerGoal = (SKShapeNode *)_goal.children[0];
    _innerGoal.physicsBody.categoryBitMask = BHSPhysicsCategoryGoal;

    // Add obstacles.
    for (SKShapeNode *_obstacle in level.obstacles) {
        SKShapeNode *obstacle = [_obstacle copy];
        obstacle.position = [self scaledPosition:obstacle.position];

        // Set up a physics body for the obstacle.
        obstacle.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:obstacle.path];
        obstacle.physicsBody.categoryBitMask = BHSPhysicsCategoryObstacle;
        obstacle.physicsBody.dynamic = NO;
        obstacle.physicsBody.affectedByGravity = NO;

        [self addChild:obstacle];
    }

    // Apply gravity (if non-standard).
    NSNumber *gravity = level.physicalProperties[BHSLevelPhysicalPropertyGravityKey];
    if (gravity) {
        self.physicsWorld.gravity = CGVectorMake(0, (CGFloat)gravity.doubleValue);
    }

    // Add a floor to the level if needed.
    if (level.hasFloor) {
        self.floor = [SKShapeNode shapeNodeWithRect:CGRectMake(0, 0, self.size.width, 10.0)];
        self.floor.fillColor = [UIColor colorWithHexRepresentation:@"#2c3e50" error:NULL];
        self.floor.strokeColor = self.floor.fillColor;
        self.floor.lineWidth = 2;

        self.floor.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:self.floor.path];
        self.floor.physicsBody.categoryBitMask = BHSPhysicsCategoryObstacle;
        self.floor.physicsBody.dynamic = NO;
        self.floor.physicsBody.affectedByGravity = NO;
        [self addChild:self.floor];
    }

    // Register for contact notifications.
    self.physicsWorld.contactDelegate = self;
    return self;
}

#pragma mark - Applying Scene Scale
- (CGPoint)scaledPosition:(CGPoint)location
{
    location.x *= self.size.width / 480.0;
    location.y *= self.size.height / 320.0;
    return location;
}

#pragma mark - Creating Scene Components
/*!
 Creates a new \c SKShapeNode representing a ball at the given position in the
 scene.
 
 \param position The position to assign the node.
 \returns A new \c SKShapeNode object.
 */
- (SKShapeNode *)createBallAtPosition:(CGPoint)position
{
    SKShapeNode *ball = [SKShapeNode shapeNodeWithCircleOfRadius:BHSBallRadius];
    ball.fillColor = [UIColor darkGrayColor];
    ball.lineWidth = 0;
    ball.position = position;

    ball.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:ball.path];
    ball.physicsBody.categoryBitMask = BHSPhysicsCategoryBall;
    ball.physicsBody.collisionBitMask = BHSPhysicsCategoryObstacle;
    ball.physicsBody.contactTestBitMask = BHSPhysicsCategoryObstacle | BHSPhysicsCategoryGoal;
    ball.physicsBody.dynamic = YES;
    ball.physicsBody.affectedByGravity = YES;

    // Levels may apply a custom bounciness to the ball; take that into account
    // here.
    NSNumber *restitution = self.currentLevel.physicalProperties[BHSLevelPhysicalPropertyBallRestitutionKey];
    if (restitution) {
        ball.physicsBody.restitution = (CGFloat)restitution.doubleValue;
    } else {
        ball.physicsBody.restitution = 0.9f;
    }

    return ball;
}

#pragma mark - User Interaction
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // We want to know if any of the touches that happened here fall within our
    // launch area; if so, hold on to that first touch (we'll ignore the rest).
    BOOL foundTouch = NO;
    CGPoint location = CGPointZero;
    for (UITouch *touch in touches) {
        location = [touch locationInNode:self];
        if ([self.launchArea containsPoint:location]) {
            foundTouch = YES;

            [self.ball removeFromParent];
            self.currentTouch = touch;
            break;
        }
    }

    // None of the touches fell within our launch area; send these touches along
    // to the next responder.
    if (!foundTouch) {
        return [self.nextResponder touchesBegan:touches withEvent:event];
    }

    // Pause the so the user can aim and show them the ball.
    self.paused = YES;
    self.ball = [self createBallAtPosition:location];
    [self addChild:self.ball];
}

static inline CGPoint BHSPointTrajectory(CGPoint initialPosition, CGVector velocity, CGVector acceleration, NSInteger time)
{
    CGFloat factor = 0.01f;
    initialPosition.x += velocity.dx * time * factor;
    initialPosition.y += velocity.dy * time * factor + acceleration.dy * time * time * factor;
    return initialPosition;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // This is part of a new touch gesture; send the touches along and ignore.
    if (!self.currentTouch) {
        return [self.nextResponder touchesMoved:touches withEvent:event];
    }

    CGPoint origin = self.ball.position;
    CGPoint location = [self.currentTouch locationInNode:self];

    // If the user drags outside of the launch area, we need to figure out where
    // the line segment between the ball and the current touch location is
    // clipped by the launch area (so we can calculate & draw a trajectory).
    // This only needs to happen if the user is not touching within the launch
    // area.
    if (![self.launchArea containsPoint:location]) {
        // What we can do is perform a binary search along this line segment,
        // moving from the ball position (within the launch area) outwards
        // toward where the user is currently touching.
        CGPoint start = origin;
        CGPoint end = location;

        // Points are "equal enough" when they're within half a pixel of one
        // another. As long as the current midpoint is still in the launch area,
        // move closer to the end location.
        while (!BHSPointsApproximatelyEqual(start, end)) {
            CGPoint midpoint = CGPointMake((start.x + end.x) / 2,
                                           (start.y + end.y) / 2);

            if ([self.launchArea containsPoint:midpoint]) {
                start = midpoint;
            } else {
                end = midpoint;
            }
        }

        // Found the clip point. Rounding to the nearest half pixel gets us
        // useful values for drawing.
        location = CGPointMake((CGFloat)round(start.x * 2) / 2, (CGFloat)round(start.y * 2) / 2);
    }

    CGVector velocity = CGVectorMake(10 * (origin.x - location.x), 10 *(origin.y - location.y));
    self.ball.physicsBody.velocity = velocity;

    static NSInteger const trajectorySize = 20;
    CGPoint trajectoryPoints[trajectorySize + 2] = {location, origin};
    for (NSInteger t = 0; t < trajectorySize; t += 1) {
        trajectoryPoints[t + 2] = BHSPointTrajectory(origin, velocity, self.physicsWorld.gravity, t);
    }

    [self.trajectory removeFromParent];
    self.trajectory = [SKShapeNode shapeNodeWithPoints:trajectoryPoints count:trajectorySize + 2];
    self.trajectory.strokeColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    self.trajectory.lineCap = kCGLineCapRound;
    self.trajectory.lineWidth = 3;
    self.trajectory.zPosition = -1;
    [self addChild:self.trajectory];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // The user has released their touch. The game should unpause to send the
    // ball flying.
    self.currentTouch = nil;
    [self.trajectory removeFromParent];
    self.paused = NO;
}

#pragma mark - SKPhysicsDelegate Methods
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if (contact.bodyA.categoryBitMask == BHSPhysicsCategoryGoal || contact.bodyB.categoryBitMask == BHSPhysicsCategoryGoal) {
        self.trackBallLocation = YES;
    } else if (contact.bodyA.categoryBitMask == BHSPhysicsCategoryBall || contact.bodyB.categoryBitMask == BHSPhysicsCategoryBall) {
        [self.ball runAction:[SKAction playSoundFileNamed:@"tap.caf" waitForCompletion:NO]];
    }
}

- (void)didEndContact:(SKPhysicsContact *)contact
{
    if (contact.bodyA.categoryBitMask == BHSPhysicsCategoryGoal || contact.bodyB.categoryBitMask == BHSPhysicsCategoryGoal) {
        self.trackBallLocation = NO;
    }
}

#pragma mark - Frame by Frame Updates
- (void)update:(CFTimeInterval)currentTime
{
    if (self.trackBallLocation) {
        CGPoint ballInBoxCoordinates = [self.goal convertPoint:self.ball.position fromNode:self];
        if ([self.innerGoal containsPoint:ballInBoxCoordinates]) {
            self.userInteractionEnabled = NO;

            [self.ball runAction:[SKAction fadeOutWithDuration:0.05] completion:^{
                [self.ball removeFromParent];
                self.trackBallLocation = NO;

                SKEmitterNode *confetti = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"Confetti" ofType:@"sks"]];
                confetti.alpha = 1.0;
                confetti.targetNode = self.innerGoal;
                [self.goal addChild:confetti];
                [self.ball runAction:[SKAction playSoundFileNamed:@"success.caf" waitForCompletion:NO]];

                NSTimeInterval totalTime = confetti.numParticlesToEmit / confetti.particleBirthRate + confetti.particleLifetime + confetti.particleLifetimeRange / 2;
                [confetti runAction:[SKAction waitForDuration:totalTime] completion:^{
                    [self.gameDelegate levelInSceneDidEnd:self];
                }];
            }];
        }
    }
}

@end
