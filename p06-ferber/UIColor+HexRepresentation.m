#import "UIColor+HexRepresentation.h"
#import "BHSErrors.h"

@implementation UIColor (HexRepresentation)

#pragma mark - Creation
+ (UIColor *)colorWithHexRepresentation:(NSString *)representation error:(NSError * __autoreleasing *)error
{
    if (!representation) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"Given color must not be nil."
                                     userInfo:nil];
    }

    NSScanner *prefixScanner = [NSScanner scannerWithString:representation];
    if ([representation hasPrefix:@"0x"]) {
        prefixScanner.scanLocation = 2;
    } else if ([representation hasPrefix:@"#"]) {
        prefixScanner.scanLocation = 1;
    }

    NSCharacterSet *hexCharacters = [NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefABCDEF"];
    NSString *validCharacters = nil;
    [prefixScanner scanCharactersFromSet:hexCharacters intoString:&validCharacters];

    if (validCharacters.length != 6) {
        if (error) {
            *error = [NSError errorWithDomain:BHSErrorDomain
                                         code:BHSErrorInvalidFormat
                                     userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected given hex representation to be six characters long. Got '%@' instead.", validCharacters]}];
        }

        return nil;
    }

    NSScanner *valueScanner = [NSScanner scannerWithString:validCharacters];
    unsigned long long hexValue = 0;
    BOOL success = [valueScanner scanHexLongLong:&hexValue];
    NSAssert(success, @"Hex conversion must succeed after the string is ensured to be six hex characters.");

    CGFloat red   = ((hexValue & 0xFF0000) >> 16) / 255.0f,
            green = ((hexValue & 0x00FF00) >> 8)  / 255.0f,
            blue  = ((hexValue & 0x0000FF) >> 0)  / 255.0f;

    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

@end
