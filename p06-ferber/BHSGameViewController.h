@import UIKit;
@import SpriteKit;
#import "BHSGameDelegate.h"

//! The view controller responsible for presenting the game on-screen.
@interface BHSGameViewController : UIViewController <BHSGameDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

#pragma mark - Properties
//! The view game scenes appear in.
@property IBOutlet SKView *gameView;

//! The button that presents the level picker view.
@property IBOutlet UIButton *levelPickerButton;

//! The view that shows level options to the user.
@property IBOutlet UICollectionView *levelPicker;

#pragma mark - Actions
- (IBAction)presentLevelPicker:(UIButton *)button;

@end
