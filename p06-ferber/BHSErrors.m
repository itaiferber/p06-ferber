#import "BHSErrors.h"

#pragma mark Error Domains
NSString * const BHSErrorDomain = @"BHSErrorDomain";

#pragma mark - Error Codes
NSInteger const BHSErrorInvalidType = 1;
NSInteger const BHSErrorInvalidFormat = 2;
