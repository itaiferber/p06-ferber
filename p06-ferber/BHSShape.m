#import "BHSShape.h"
#import "BHSErrors.h"
#import "UIColor+HexRepresentation.h"

@implementation BHSShape

#pragma mark - MTLJSONSerializing Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"type": @"type",
             @"size": @"size",
             @"center": @"center",
             @"rotation": @"rotation",
             @"strokeColor": @"stroke_color",
             @"fillColor": @"fill_color",
             @"lineWidth": @"line_width"};
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString:@"type"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *type, BOOL *success, NSError * __autoreleasing *error) {
            if (![type isKindOfClass:[NSString class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected type to be an NSString instance; got %@ instead.", NSStringFromClass([type class])]}];
                }

                return nil;
            }
            
            if ([type isEqualToString:@"goal"]) {
                return @(BHSShapeTypeGoal);
            } else if ([type isEqualToString:@"rect"]) {
                return @(BHSShapeTypeRect);
            } else if ([type isEqualToString:@"ellipse"]) {
                return @(BHSShapeTypeEllipse);
            } else {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidFormat
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected shape type to be 'rect' or 'ellipse'; got '%@' instead.", type]}];
                }

                return nil;
            }
        }];
    } else if ([key isEqualToString:@"size"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *value, BOOL *success, NSError * __autoreleasing *error) {
            if (![value isKindOfClass:[NSString class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected size to be an NSString instance; got %@ instead.", NSStringFromClass([value class])]}];
                }

                return nil;
            }

            CGSize size = CGSizeFromString(value);
            if (size.width == 0 || size.height == 0) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidFormat
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected size to have non-zero dimension; got %@ instead.", NSStringFromCGSize(size)]}];
                }
                
                return nil;
            }

            return [NSValue valueWithCGSize:size];
        }];
    } else if ([key isEqualToString:@"center"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *value, BOOL *success, NSError * __autoreleasing *error) {
            if (![value isKindOfClass:[NSString class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected center to be an NSString instance; got %@ instead.", NSStringFromClass([value class])]}];
                }

                return nil;
            }

            CGPoint center = CGPointFromString(value);
            return [NSValue valueWithCGPoint:center];
        }];
    } else if ([key isEqualToString:@"rotation"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSNumber *value, BOOL *success, NSError * __autoreleasing *error) {
            if (![value isKindOfClass:[NSNumber class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected size to be an NSNumber instance; got %@ instead.", NSStringFromClass([value class])]}];
                }
                
                return nil;
            }

            return @(value.doubleValue / 180.0 * M_PI);
        }];
    } else if ([key isEqualToString:@"strokeColor"] || [key isEqualToString:@"fillColor"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *value, BOOL *success, NSError * __autoreleasing *error) {
            if (!value) {
                *success = YES;
                return nil;
            }

            UIColor *color = [UIColor colorWithHexRepresentation:value error:error];
            *success = color != nil;
            return color;
        }];
    } else if ([key isEqualToString:@"lineWidth"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSNumber *value, BOOL *success, NSError * __autoreleasing *error) {
            if (![value isKindOfClass:[NSNumber class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected line width to be an NSNumber instance; got %@ instead.", NSStringFromClass([value class])]}];
                }

                return nil;
            }

            return value;
        }];
    }

    return nil;
}

@end
