@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (HexRepresentation)

#pragma mark - Creation
/*!
 Creates a new UIColor instance with the given hex representation string. The
 given string may begin with an optional "#", or an optional "0x", but must be
 six characters long.
 
 \param representation The hex representation of the color as a string. Must not
                       be \c nil.
 \param error An optional error parameter to write into on parse failure.
 \returns A new UIColor with the given representation, or \c nil if the given
          representation is not a valid string.
 \throws \c NSInvalidArgumentException if the given representation is \c nil.
 */
+ (UIColor *)colorWithHexRepresentation:(NSString *)representation error:(NSError * __autoreleasing *)error;

@end

NS_ASSUME_NONNULL_END
