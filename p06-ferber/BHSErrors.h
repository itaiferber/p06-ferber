@import Foundation;

#pragma mark Error Domains
//! An error domain for NSErrors returned in BHS code.
extern NSString * const BHSErrorDomain;

#pragma mark - Error Codes
/*!
 An error code which corresponds to receiving a data object of an unexpected
 type.
 */
extern NSInteger const BHSErrorInvalidType;

//! An error code which corresponds to receiving data in an unexpected format.
extern NSInteger const BHSErrorInvalidFormat;
