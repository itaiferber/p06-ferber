//
//  main.m
//  p06-ferber
//
//  Created by Itai Ferber on 3/2/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BHSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BHSAppDelegate class]));
    }
}
