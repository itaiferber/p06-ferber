@import UIKit;
@import Mantle;

NS_ASSUME_NONNULL_BEGIN

#pragma mark Shape Types
typedef NS_ENUM(NSInteger, BHSShapeType) {
    BHSShapeTypeRect,
    BHSShapeTypeEllipse,
    BHSShapeTypeGoal
};

#pragma mark -
@interface BHSShape : MTLModel <MTLJSONSerializing>

#pragma mark - Properties
//! The receiver's type.
@property (readonly) BHSShapeType type;

//! The receiver's size.
@property (readonly) CGSize size;

//! The receiver's center.
@property (readonly) CGPoint center;

//! The receiver's rotation.
@property (readonly) CGFloat rotation;

//! The receiver's stroke color.
@property (readonly, nullable) UIColor *strokeColor;

//! The receiver's fill color.
@property (readonly, nullable) UIColor *fillColor;

//! The receiver's line width.
@property (readonly) CGFloat lineWidth;

@end

NS_ASSUME_NONNULL_END
