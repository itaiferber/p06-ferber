@import Foundation;
@import SpriteKit;
@import Mantle;

NS_ASSUME_NONNULL_BEGIN

#pragma mark Physical Property Keys
/*!
 A key corresponding to an optional level gravity property in the level's
 physical properties dictionary.
 */
extern NSString * const BHSLevelPhysicalPropertyGravityKey;

/*!
 A key corresponding to an optional ball restitution (bounciness) in the level's
 physical properties dictionary.
 */
extern NSString * const BHSLevelPhysicalPropertyBallRestitutionKey;

#pragma mark -
@interface BHSLevel : MTLModel <MTLJSONSerializing>

#pragma mark - Properties
//! The background color for the level.
@property (readonly) UIColor *backgroundColor;

//! Whether the level should have a floor spanning its width or not.
@property (readonly) BOOL hasFloor;

/*!
 Any special physical properties applied to the level. Mapping between a key
 denoting a physical property, and the numeric value to set it to.
 */
@property (readonly) NSDictionary<NSString *, NSNumber *> *physicalProperties;

//! A shape node corresponding to where the ball should be launched from.
@property (readonly) SKShapeNode *launchArea;

//! A shape node corresponding to the level's goal box.
@property (readonly) SKShapeNode *goal;

//! The obstacles present in the level.
@property (readonly) NSArray<SKShapeNode *> *obstacles;

#pragma mark - Initialization
/*!
 Returns an array containing the levels stored in a JSON file at the given URL.
 
 \param url The URL at which the JSON configuration file is located.
            Must not be nil.
 \param error An optional error parameter to load into in case of error.
 \returns An array of level objects read from the given file, or \c nil on
          failure.
 \throws \c NSInvalidArgumentException if the given URL is \c nil.
 */
+ (NSArray<BHSLevel *> *)levelsWithContentsAtURL:(NSURL *)url error:(NSError * _Nullable __autoreleasing * _Nullable)error;

@end

NS_ASSUME_NONNULL_END
