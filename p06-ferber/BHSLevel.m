#import "BHSLevel.h"
#import "BHSShape.h"
#import "BHSErrors.h"
#import "UIColor+HexRepresentation.h"

#pragma mark Physical Property Keys
NSString * const BHSLevelPhysicalPropertyGravityKey = @"gravity";
NSString * const BHSLevelPhysicalPropertyBallRestitutionKey = @"restitution";

#pragma mark -
@implementation BHSLevel

#pragma mark - Initialization
+ (NSArray<BHSLevel *> *)levelsWithContentsAtURL:(NSURL *)url error:(NSError * __autoreleasing *)error
{
    if (!url) {
        @throw NSInvalidArgumentException;
    }

    NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:error];
    if (!data) {
        return nil;
    }

    id topLevelObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    if (!topLevelObject) {
        return nil;
    }

    NSMutableArray *levels = [NSMutableArray array];
    for (NSDictionary *representation in (NSArray *)topLevelObject) {
        BHSLevel *level = [MTLJSONAdapter modelOfClass:[BHSLevel class] fromJSONDictionary:representation error:error];
        if (!level) {
            return nil;
        }

        [levels addObject:level];
    }

    return levels;
}

#pragma mark - MLTJSONSerializing Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"backgroundColor": @"background_color",
             @"hasFloor": @"has_floor",
             @"physicalProperties": @"physical_properties",
             @"launchArea": @"launch_area",
             @"goal": @"goal",
             @"obstacles": @"obstacles"};
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString:@"backgroundColor"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *value, BOOL *success, NSError * __autoreleasing *error) {
            if (![value isKindOfClass:[NSString class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected background color to be an NSString instance; got %@ instead.", NSStringFromClass([value class])]}];
                }

                return nil;
            }

            UIColor *color = [UIColor colorWithHexRepresentation:value error:error];
            *success = color != nil;
            return color;
        }];
    } else if ([key isEqualToString:@"physicalProperties"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *properties, BOOL *success, NSError * __autoreleasing *error) {
            if (![properties isKindOfClass:[NSDictionary class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected properties to be an NSDictionary instance; got %@ instead.", NSStringFromClass([properties class])]}];
                }

                return nil;
            }

            __block NSString *errorMessage = nil;
            [properties enumerateKeysAndObjectsUsingBlock:^(id k, id v, BOOL *stop) {
                if (![k isKindOfClass:[NSString class]]) {
                    errorMessage = [NSString stringWithFormat:@"Expected property key to be an NSString instance; got %@ instead.", NSStringFromClass([k class])];
                    *stop = YES;
                } else if (![v isKindOfClass:[NSNumber class]]) {
                    errorMessage = [NSString stringWithFormat:@"Expected property value to be an NSNumber instance; got %@ instead.", NSStringFromClass([v class])];
                    *stop = YES;
                }
            }];

            if (errorMessage) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
                }

                return nil;
            }

            return properties;
        }];
    } else if ([key isEqualToString:@"launchArea"] || [key isEqualToString:@"goal"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *representation, BOOL *success, NSError * __autoreleasing *error) {
            BHSShape *shape = [MTLJSONAdapter modelOfClass:[BHSShape class] fromJSONDictionary:representation error:error];
            if (!shape) {
                *success = NO;
                return nil;
            }
            
            return BHSShapeNodeFromShape(shape);
        }];
    } else if ([key isEqualToString:@"obstacles"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray<NSDictionary *> *representations, BOOL *success, NSError * __autoreleasing *error) {
            if (![representations isKindOfClass:[NSArray class]]) {
                *success = NO;
                if (error) {
                    *error = [NSError errorWithDomain:BHSErrorDomain
                                                 code:BHSErrorInvalidType
                                             userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected obstacles to be an NSArray instance; got %@ instead.", NSStringFromClass([representations class])]}];
                }

                return nil;
            }

            NSMutableArray *obstacles = [NSMutableArray array];
            for (NSDictionary *representation in representations) {
                if (![representation isKindOfClass:[NSDictionary class]]) {
                    *success = NO;
                    if (error) {
                        *error = [NSError errorWithDomain:BHSErrorDomain
                                                     code:BHSErrorInvalidType
                                                 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Expected obstacle to be an NSDictionary instance; got %@ instead.", NSStringFromClass([representation class])]}];
                    }
                    
                    return nil;
                }

                BHSShape *shape = [MTLJSONAdapter modelOfClass:[BHSShape class] fromJSONDictionary:representation error:error];
                if (!shape) {
                    *success = NO;
                    return nil;
                }

                [obstacles addObject:BHSShapeNodeFromShape(shape)];
            }

            return obstacles;
        }];
    }

    return nil;
}

/*!
 Returns a new shape node from the given shape.
 
 \param shape The shape to use in creating a new node.
 \returns A new SKShapeNode representing the given shape, or \c nil if the given
          shape was \c nil.
 */
static inline SKShapeNode *BHSShapeNodeFromShape(BHSShape * _Nullable shape)
{
    if (!shape) {
        return nil;
    }

    CGRect bounds = CGRectZero;
    bounds.size = shape.size;

    UIBezierPath *path = nil;
    switch (shape.type) {
        case BHSShapeTypeRect:
            path = [UIBezierPath bezierPathWithRect:bounds];
            break;

        case BHSShapeTypeEllipse:
            path = [UIBezierPath bezierPathWithOvalInRect:bounds];
            break;
        case BHSShapeTypeGoal:
        {
            // Draw goal with wings
            path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(bounds.origin.x - bounds.size.width/2, bounds.origin.y + bounds.size.height + bounds.size.height/3)];
            [path addLineToPoint:CGPointMake(bounds.origin.x, bounds.origin.y + bounds.size.height)];
            [path addLineToPoint:CGPointMake(bounds.origin.x, bounds.origin.y)];
            [path addLineToPoint:CGPointMake(bounds.origin.x + bounds.size.width, bounds.origin.y)];
            [path addLineToPoint:CGPointMake(bounds.origin.x + bounds.size.width, bounds.origin.y + bounds.size.height)];
            [path addLineToPoint:CGPointMake(bounds.origin.x + bounds.size.width + bounds.size.width/2, bounds.origin.y + bounds.size.height + bounds.size.height/3)];
            [path moveToPoint:CGPointMake(bounds.origin.x - bounds.size.width/2, bounds.origin.y + bounds.size.height + bounds.size.height/3)];
            [path closePath];
            break;
        }
    }

    SKShapeNode *node = [SKShapeNode shapeNodeWithPath:path.CGPath centered:YES];
    node.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:node.path];
    node.physicsBody.dynamic = NO;
    node.physicsBody.affectedByGravity = NO;
    
    // Add box as a child to goal for detecting victory
    if (shape.type == BHSShapeTypeGoal) {
        SKShapeNode *contactBox = [SKShapeNode shapeNodeWithRect:CGRectInset(bounds, 2, 2)];
        contactBox.lineWidth = 0;
        
        contactBox.position = CGPointMake(-bounds.size.width / 2, -bounds.size.height*2/3);
        contactBox.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:contactBox.path];
        contactBox.physicsBody.dynamic = NO;
        contactBox.physicsBody.affectedByGravity = NO;
        
        [node addChild:contactBox];
    }

    node.position = shape.center;
    node.zRotation = shape.rotation;

    if (shape.strokeColor) {
        node.strokeColor = shape.strokeColor;
    }

    if (shape.fillColor) {
        node.fillColor = shape.fillColor;
    }

    if (shape.lineWidth > 0) {
        node.lineWidth = shape.lineWidth;
    } else {
        node.lineWidth = 3.0;
    }
    
    return node;
}

@end
