@import UIKit;

//! The application's delegate.
@interface BHSAppDelegate : UIResponder <UIApplicationDelegate>

#pragma mark - Properties
//! The main application window.
@property (nonatomic) IBOutlet UIWindow *window;

@end
