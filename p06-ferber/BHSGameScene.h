#import "BHSLevel.h"
#import "BHSGameDelegate.h"
@import SpriteKit;

NS_ASSUME_NONNULL_BEGIN

@interface BHSGameScene : SKScene <SKPhysicsContactDelegate>

#pragma mark - Properties
//! The current level being presented in the scene.
@property (readonly) BHSLevel *currentLevel;

//! The delegate to inform once the game ends.
@property (weak, nullable) id<BHSGameDelegate> gameDelegate;

#pragma mark - Initialization
/*!
 Creates a new scene object with the given level.

 \param size The size to give the level.
 \param level The level to load and display. Must not be \c nil.
 \returns A new scene object displaying the given level.
 \throws \c NSInvalidArgumentException if \c level is \c nil.
 */
+ (instancetype)sceneWithSize:(CGSize)size level:(BHSLevel *)level;

/*!
 Initializes the receiver with the given level.
 
 \param size The size to give the level.
 \param level The level to load and display. Must not be \c nil.
 \returns An initialized scene object displaing the given level.
 \throws \c NSInvalidArgumentException if \c level is \c nil.
 */
- (instancetype)initWithSize:(CGSize)size level:(BHSLevel *)level;

@end

NS_ASSUME_NONNULL_END
